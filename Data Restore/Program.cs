﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Data.SqlClient;
using MySql.Data.MySqlClient;
using System.Data;
using System.IO;

/*
Notes:
-Make sure store DB is latest to prevent replicated data inconsistencies
-Set the appropriate connection settings
-Set the omit list if applicable
*/

namespace Data_Restore
{
    class Program
    {
        static string TABLE_FILE = string.Concat(AppDomain.CurrentDomain.BaseDirectory, "TABLES.txt");
        static string INSERT_FILE = string.Concat(AppDomain.CurrentDomain.BaseDirectory, "MISSING_STORE_DATA_INSERT.sql");

        //store connection settings
        static string STORE_SERVER = Properties.Settings.Default.STORE_SERVER;
        static string STORE_DB_NAME = Properties.Settings.Default.STORE_DB_NAME;
        static string STORE_DB_USER = Properties.Settings.Default.STORE_DB_USER;
        static string STORE_DB_PASS = Properties.Settings.Default.STORE_DB_PASS;

        //central server connection settings
        static string CENTRAL_SERVER = Properties.Settings.Default.CENTRAL_SERVER;
        static string CENTRAL_DB_NAME = Properties.Settings.Default.CENTRAL_DB_NAME;
        static string CENTRAL_DB_USER = Properties.Settings.Default.CENTRAL_DB_USER;
        static string CENTRAL_DB_PASS = Properties.Settings.Default.CENTRAL_DB_PASS;

        static void Main(string[] args)
        {
            int MissingCount = 0;

            #region Omit List
            List<string> OmitTables = new List<string>();
            string[] olist = Properties.Settings.Default.OMIT_TABLES.Split(',');

            foreach (var item in olist)
            {
                OmitTables.Add(item.ToLower());
            }            
            #endregion

            DataTable store_data = new DataTable();
            DataTable store_tables_without_pk = new DataTable();
            DataTable central_data = new DataTable();

            Console.WriteLine("Starting Data Check...");

            string connSTORE = $"Server={STORE_SERVER};Database={STORE_DB_NAME};User={STORE_DB_USER};Password={STORE_DB_PASS}";
            string connCENTRAL = $"server={CENTRAL_SERVER};database={CENTRAL_DB_NAME};uid={CENTRAL_DB_USER};pwd={CENTRAL_DB_PASS}";

            try
            {
                #region Clear Old Files
                //clear files
                if (File.Exists(TABLE_FILE))
                {
                    File.Delete(TABLE_FILE);
                }
                if (File.Exists(INSERT_FILE))
                {
                    File.Delete(INSERT_FILE);
                }
                #endregion

                //STORE
                SqlConnection connection = new SqlConnection(connSTORE);
                connection.Open();

                //CENTRAL SERVER
                MySqlConnection cnn = new MySqlConnection(connCENTRAL);
                cnn.Open();

                #region Generate Table List
                //get central server tables
                List<string> CentralTables = new List<string>();
                MySqlCommand cmd = new MySqlCommand($"SHOW TABLES FROM `{CENTRAL_DB_NAME}`", cnn);
                cmd.CommandTimeout = 300;
                MySqlDataAdapter central_dataAdapter = new MySqlDataAdapter(cmd);
                central_dataAdapter.Fill(central_data);

                for (int i = 0; i < central_data.Rows.Count; i++)
                {
                    CentralTables.Add(central_data.Rows[i][0].ToString());
                }

                //get store tables
                List<string[]> StoreTables = new List<string[]>();
                SqlCommand command = new SqlCommand($"select table_name, column_name " +
                                                    $"from information_schema.columns " +
                                                    $"where table_schema = 'dbo' " +
                                                    $"and table_name in (select TABLE_NAME from INFORMATION_SCHEMA.TABLES where TABLE_TYPE = 'BASE TABLE' and TABLE_CATALOG = '{STORE_DB_NAME}') " +
                                                    $"and ordinal_position < 2 order by column_name asc", connection);
                command.CommandTimeout = 300;
                SqlDataAdapter store_dataAdapter = new SqlDataAdapter(command);
                store_dataAdapter.Fill(store_data);

                //get store tables without primary keys
                command = new SqlCommand($"select tab.[name] as table_name " +
                                          "from sys.tables tab " +
                                              "left outer join sys.indexes pk " +
                                                  "on tab.object_id = pk.object_id " +
                                                  "and pk.is_primary_key = 1 " +
                                          "where pk.object_id is null " +
                                          "order by schema_name(tab.schema_id), " +
                                              "tab.[name]", connection);
                command.CommandTimeout = 300;
                store_dataAdapter = new SqlDataAdapter(command);
                store_dataAdapter.Fill(store_tables_without_pk);

                //update omit list
                for (int i = 0; i < store_tables_without_pk.Rows.Count; i++)
                {
                    OmitTables.Add(store_tables_without_pk.Rows[i][0].ToString().ToLower());
                }

                //create final table list
                for (int i = 0; i < store_data.Rows.Count; i++)
                {
                    if (CentralTables.Contains(store_data.Rows[i][0].ToString().ToLower()))
                    {
                        //skip if in omit list
                        if (!OmitTables.Contains(store_data.Rows[i][0].ToString().ToLower()))
                        {
                            StoreTables.Add(new string[] { store_data.Rows[i][0].ToString(), store_data.Rows[i][1].ToString() });
                        }
                    }
                }

                Console.WriteLine($"Found {StoreTables.Count} tables");
                #endregion                

                foreach (string[] TableDefinitions in StoreTables)
                {
                    Console.WriteLine($"[{DateTime.Now}] - Checking {TableDefinitions[0]}...");

                    store_data = new DataTable();
                    central_data = new DataTable();

                    string IN_ = "";
                    MissingCount = 0;

                    #region Pull All Data From Store and Central Server
                    //STORE
                    command = new SqlCommand($"SELECT [{TableDefinitions[1]}] FROM [dbo].[{TableDefinitions[0]}]", connection);
                    command.CommandTimeout = 300;
                    store_dataAdapter = new SqlDataAdapter(command);
                    store_dataAdapter.Fill(store_data);

                    //CENTRAL SERVER
                    cmd = new MySqlCommand($"SELECT `{TableDefinitions[1]}` FROM `{TableDefinitions[0]}`", cnn);
                    cmd.CommandTimeout = 300;
                    central_dataAdapter = new MySqlDataAdapter(cmd);
                    central_dataAdapter.Fill(central_data);
                    #endregion

                    List<string> st = new List<string>();
                    List<string> cs = new List<string>();

                    #region Create IN Statement To Pull Missing Records Only
                    //save to list for speed on larger data sets
                    if (central_data.Rows.Count > 0)
                    {
                        for (int a = 0; a < central_data.Rows.Count; a++)
                        {
                            cs.Add(central_data.Rows[a][0].ToString());
                        }
                    }
                    if (store_data.Rows.Count > 0)
                    {
                        for (int a = 0; a < store_data.Rows.Count; a++)
                        {
                            st.Add(store_data.Rows[a][0].ToString());
                        }
                    }

                    var MissingRecordID = cs.Except(st);
                    MissingCount = MissingRecordID.Count();

                    foreach (var id in MissingRecordID)
                    {
                        IN_ += string.Concat((IN_ != "" ? "," : ""), id);
                    }
                    #endregion

                    if (MissingCount > 0)
                    {
                        SaveText1(TableDefinitions[0] + " [" + MissingCount.ToString() + "]");

                        var INSERT_MAIN = new StringBuilder();
                        var COLUMNS = new StringBuilder();
                        var RECORD = new StringBuilder();
                        var RECORD_DATA = new StringBuilder();
                        string _RECORD = "";
                        int InsertRecordCount = 0;

                        central_data = new DataTable();

                        #region Pull Missing Values From Central Server
                        //CENTRAL SERVER PULL ONLY MISSING VALUES
                        cmd.Dispose();
                        cmd = new MySqlCommand($"SELECT * FROM `{TableDefinitions[0]}` WHERE {TableDefinitions[1]} IN ({IN_})", cnn)
                        {
                            CommandTimeout = 300
                        };
                        central_dataAdapter.Dispose();
                        central_dataAdapter = new MySqlDataAdapter(cmd);
                        central_dataAdapter.Fill(central_data);
                        #endregion

                        SaveText($"/******{TableDefinitions[0]} - START*****/" + Environment.NewLine);

                        #region Disable Trigger
                        SaveText(string.Concat($"DISABLE Trigger ALL ON {TableDefinitions[0]}", Environment.NewLine));
                        #endregion

                        #region Start Insert Statement
                        SaveText(string.Concat($"SET IDENTITY_INSERT [dbo].[{TableDefinitions[0]}] ON", Environment.NewLine));
                        SaveText(string.Concat($"INSERT INTO [dbo].[{TableDefinitions[0]}]", Environment.NewLine));

                        //COLUMNS
                        foreach (DataColumn col in central_data.Columns)
                        {
                            COLUMNS.Append(string.Concat((COLUMNS.Length != 0 ? "," : ""), "[", col.ColumnName, "]"));
                        }
                        SaveText(string.Concat($"({COLUMNS}) VALUES", Environment.NewLine));
                        #endregion

                        #region Create Insert Records
                        //RECORDS
                        foreach (DataRow row in central_data.Rows)
                        {
                            RECORD = new StringBuilder();
                            RECORD_DATA = new StringBuilder();

                            #region Create New Record Insert Statement (1000 Records)
                            if (InsertRecordCount == 999)
                            {
                                _RECORD = "";
                                SaveText(Environment.NewLine);
                                SaveText(string.Concat($"INSERT INTO [dbo].[{TableDefinitions[0]}]", Environment.NewLine));

                                //COLUMNS                                
                                SaveText(string.Concat($"({COLUMNS}) VALUES", Environment.NewLine));

                                InsertRecordCount = 0;
                            }
                            #endregion

                            #region Create Record
                            //create RECORD
                            for (int a = 0; a < row.Table.Columns.Count; a++)
                            {
                                if (row[a] == DBNull.Value)
                                {
                                    RECORD_DATA.Append(RECORD_DATA.Length != 0 ? "," : "");
                                    RECORD_DATA.Append("");
                                    RECORD_DATA.Append("NULL");
                                    RECORD_DATA.Append("");
                                }
                                else if (row.Table.Columns[a].DataType == typeof(string))
                                {
                                    RECORD_DATA.Append(RECORD_DATA.Length != 0 ? "," : "");
                                    RECORD_DATA.Append("'");
                                    RECORD_DATA.Append(row[a].ToString().Replace("'", "''"));
                                    RECORD_DATA.Append("'");
                                }
                                else if (row.Table.Columns[a].DataType == typeof(DateTime))
                                {
                                    RECORD_DATA.Append((RECORD_DATA.Length != 0 ? "," : ""));
                                    RECORD_DATA.Append("CAST('");
                                    RECORD_DATA.Append(((DateTime)row[a]).ToString("yyyy-MM-dd HH:mm:ss.fff"));
                                    RECORD_DATA.Append("' AS DATETIME)");
                                }
                                else
                                {
                                    RECORD_DATA.Append((RECORD_DATA.Length != 0 ? "," : ""));
                                    RECORD_DATA.Append("");
                                    RECORD_DATA.Append(row[a]);
                                    RECORD_DATA.Append("");
                                }
                            }

                            _RECORD = string.Concat((_RECORD != string.Empty ? "," : ""), "(", RECORD_DATA.ToString(), ")");

                            SaveText(_RECORD);
                            #endregion

                            InsertRecordCount++;
                        }
                        #endregion

                        #region Finalise Insert Statement
                        SaveText(string.Concat($"SET IDENTITY_INSERT [dbo].[{TableDefinitions[0]}] OFF", Environment.NewLine));
                        #endregion

                        #region Enable Trigger
                        SaveText(string.Concat($"ENABLE Trigger ALL ON {TableDefinitions[0]}", Environment.NewLine));
                        #endregion

                        SaveText($"/******{TableDefinitions[0]} - END*****/" + Environment.NewLine);
                    }                    

                    Console.WriteLine($"[{DateTime.Now}] - Missing {MissingCount}");
                    Console.WriteLine("----------------------------");
                }

                #region ENABLE ALL TRIGGERS
                SaveText(string.Concat($"ENABLE Trigger ALL ON ALL SERVER;", Environment.NewLine));
                SaveText(string.Concat($"GO", Environment.NewLine));
                #endregion

                connection.Close();
                cnn.Close();
            }
            catch (Exception ex)
            {
                Console.WriteLine($"[{DateTime.Now}] - There was an error: {ex.ToString()}");
            }

            Console.WriteLine("Press any key to continue...");
            Console.ReadKey();
        }

        static void SaveText(string str)
        {
            try
            {
                FileInfo fi = new FileInfo(INSERT_FILE);

                if (!File.Exists(INSERT_FILE))
                {
                    using (StreamWriter sw = fi.CreateText())
                    {
                        sw.WriteLine("/****** Auto generated Insert script ******/");
                    }
                }

                using (StreamWriter sw = fi.AppendText())
                {
                    sw.WriteLine(str);
                }
            }
            catch (Exception)
            {
            }
        }
        static void SaveText1(string str)
        {
            try
            {
                FileInfo fi = new FileInfo(TABLE_FILE);

                if (!File.Exists(TABLE_FILE))
                {
                    using (StreamWriter sw = fi.CreateText())
                    {
                        sw.WriteLine("/****** Auto generated Insert script ******/");
                    }
                }

                using (StreamWriter sw = fi.AppendText())
                {
                    sw.WriteLine(str);
                }
            }
            catch (Exception)
            {
            }
        }        
    }
}
